## About The Project
This is the solution from problem case in Keda Entrace Test that I received via email at February 16th 2024.

## Getting Started
As described on the problem case, this solution run with framework Odoo 14. Please set up your Odoo 14 properly including prepare the database, install python modules, etc. Then you can continue to the next step.

### Installation
1. Clone this repo to a folder, I used to create folder custom-addons, inside Odoo 14 project environment.
2. Copy the absolute directory of folder custom-addons.
3. Paste the absolute directory to file odoo configuration, section addons_path.
4. Restart your Odoo.
5. If you put it on existing Odoo, in menu Apps you need to click menu Update App List.
6. Install module Keda Materials.

## Usage
In Odoo web interface, you will have new menu Materials and you can do CRUD operation there.
To use rest API, you can import postman collection in this repo then see the example provided.

