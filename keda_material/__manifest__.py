{
    'name': 'KEDA Material',
    'version': '14.0.0.1',
    'summary': 'This is the solution of entrance test from KEDA',
    'depends': [
        'rest_api'
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/keda_material_view.xml'
    ],
    'installable': True,
}