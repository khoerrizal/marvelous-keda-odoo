import json
import traceback

from odoo import http
from odoo.http import request
from odoo.addons.rest_api.controllers.main import (
    check_permissions,
    successful_response,
    error_response,
    rest_cors_value,
)


class MaterialController(http.Controller):
    def __init__(self):
        self.model_name = 'keda.materials'
        self.default_fields = [
            "id",
            "name",
            "code",
            "material_type",
            "buy_price",
            "supplier_id"
        ]

    @http.route(
        '/material',
        type='http', auth='none', methods=["GET"], csrf=False, cors=rest_cors_value)
    # @check_permissions
    def get_material_list(self, *args, **post):
        q = request.params.get('q')
        filter_type = request.params.get('material_type', '')
        offset = int(request.params.get('offset', 0))
        limit = int(request.params.get('limit', 0))
        domain = []
        if q:
            domain = [
                '|',
                '|',
                ('name', 'ilike', q),
                ('code', 'ilike', q),
            ]
        if filter_type:
            if filter_type not in [
                'fabric',
                'jeans',
                'cotton'
            ]:
                return error_response(500, "invalid_material_type", f"Type {filter_type} is invalid")
            domain.extend([('material_type', '=', filter_type)])
        total = request.env[self.model_name].with_user(1).search_count(domain)
        items = request.env[self.model_name].with_user(1).search_read(
            domain,
            self.default_fields,
            offset,
            limit,
            order='id desc')
        res = {
            'offset': offset,
            'limit': limit,
            'total': total,
            'items': items
        }
        return successful_response(200, res)

    @http.route(
        '/material/<int:material_id>',
        type='http', auth='none', methods=["GET"], csrf=False, cors=rest_cors_value)
    # @check_permissions
    def get_material_detail(self, material_id, *args, **post):
        domain = [('id', '=', material_id)]
        res = request.env[self.model_name].with_user(1).search_read(
            domain,
            self.default_fields,
            limit=1
        )
        if not res:
            return error_response(500, "invalid_material_id", "Material details not found")
        return successful_response(200, res[0])

    @http.route(
        '/material',
        type='http', auth='none', methods=["POST"], csrf=False, cors=rest_cors_value)
    # @check_permissions
    def post_material_create(self, *args, **post):
        try:
            data = json.loads(http.request.httprequest.data.decode())
            item = request.env[self.model_name].with_user(1).create(data)
            res = item.read(self.default_fields)[0]
            return successful_response(200, res)
        except Exception as err:
            print(traceback.format_exc())
            print("================")
            print(err)
            return error_response(500, "error", "Failed to create material")

    @http.route(
        '/material/<int:material_id>',
        type='http', auth='none', methods=["PATCH"], csrf=False, cors=rest_cors_value)
    # @check_permissions
    def patch_material_update(self, material_id, *args, **post):
        try:
            data = json.loads(http.request.httprequest.data.decode())
            item = request.env[self.model_name].with_user(1).browse(material_id)
            item.update(data)
            res = item.read(self.default_fields)[0]
            return successful_response(200, res)
        except Exception as err:
            print(traceback.format_exc())
            print("================")
            print(err)
            return error_response(500, "error", "Failed to create material")

    @http.route(
        '/material/<int:material_id>',
        type='http', auth='none', methods=["DELETE"], csrf=False, cors=rest_cors_value)
    # @check_permissions
    def delete_material(self, material_id, *args, **post):
        domain = [('id', '=', material_id)]
        item = request.env[self.model_name].with_user(1).search(
            domain,
            limit=1
        )
        if not item:
            return error_response(500, "invalid_material_id", "Material details not found")
        try:
            item.unlink()
        except Exception as err:
            print(traceback.format_exc())
            print("================")
            print(err)
            return error_response(500, "error_delete", "Failed to delete material")
        return successful_response(200, {'message': 'success'})
