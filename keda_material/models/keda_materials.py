from odoo import api, fields, models
from odoo.exceptions import UserError, AccessError, ValidationError

class Materials(models.Model):
    _name = 'keda.materials'
    _rec_name = 'name'
    _description = 'KEDA Materials'
    _order = 'id desc'

    name = fields.Char(required=True)
    code = fields.Char(required=True)
    material_type = fields.Selection(selection=[
        ('fabric', 'Fabric'),
        ('jeans', 'Jeans'),
        ('cotton', 'Cotton'),
    ], required=True, )
    buy_price = fields.Float(required=True)
    supplier_id = fields.Many2one(comodel_name="res.partner", string="Supplier", required=True, )

    @api.constrains('buy_price')
    def _check_max_buy_price(self):
        if self.buy_price < 100:
            raise ValidationError("Buy price must more than 100")
