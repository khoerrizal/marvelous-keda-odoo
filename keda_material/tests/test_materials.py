from odoo.exceptions import ValidationError
from odoo.tests import tagged
import psycopg2
from odoo.tests.common import SavepointCase


@tagged('post_install', '-at_install')
class TestMaterials(SavepointCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        Materials = cls.env['keda.materials']
        cls.supplier0 = cls.env['res.partner'].create({'name': 'Supplier A'})
        cls.material = Materials.create({
            'name': 'Test Material',
            'code': 'TM01',
            'material_type': 'fabric',
            'buy_price': 110,
            'supplier_id': cls.supplier0.id,
        })

    def test_material_more_than_max(cls):
        with cls.assertRaises(ValidationError):
            cls.env['keda.materials'].create({
                'name': 'Test Material',
                'code': 'TM01',
                'material_type': 'fabric',
                'buy_price': 10,
                'supplier_id': cls.supplier0.id,
            })

    def test_material_with_incomplete_form(cls):
        with cls.assertRaises(psycopg2.errors.NotNullViolation):
            with cls.env.cr.savepoint():
                cls.env['keda.materials'].create({
                    'code': 'TM01',
                    'material_type': 'fabric',
                    'buy_price': 110,
                    'supplier_id': cls.supplier0.id,
                })

        with cls.assertRaises(psycopg2.errors.NotNullViolation):
            with cls.env.cr.savepoint():
                cls.env['keda.materials'].create({
                    'name': 'Test Material',
                    'material_type': 'fabric',
                    'buy_price': 110,
                    'supplier_id': cls.supplier0.id,
                })

        with cls.assertRaises(psycopg2.errors.NotNullViolation):
            with cls.env.cr.savepoint():
                cls.env['keda.materials'].create({
                    'name': 'Test Material',
                    'code': 'TM01',
                    'buy_price': 110,
                    'supplier_id': cls.supplier0.id,
                })

        with cls.assertRaises(psycopg2.errors.NotNullViolation):
            with cls.env.cr.savepoint():
                cls.env['keda.materials'].create({
                    'name': 'Test Material',
                    'code': 'TM01',
                    'material_type': 'fabric',
                    'supplier_id': cls.supplier0.id,
                })

        with cls.assertRaises(psycopg2.errors.NotNullViolation):
            with cls.env.cr.savepoint():
                cls.env['keda.materials'].create({
                    'name': 'Test Material',
                    'code': 'TM01',
                    'material_type': 'fabric',
                    'buy_price': 110,
                })
